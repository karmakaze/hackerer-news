import Vue from 'vue'
import VueStash from 'vue-stash'
import VueScript2 from 'vue-script2'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

// @ts-ignore
Vue.use(VueStash)
Vue.use(VueScript2)

const aiMlRegex = '\\b(?:' + [
  'ai', 'Alpaca', 'Bard', 'Davinci', 'deepfake', 'Deep Learning', 'ChatGPT[1-9]?', 'GPT-?[1-9]?s?', 'Kaggle',
  'machine learning', 'Neural Networks?',
  'Language Models?', 'LLaMa', 'LLM', 'Midjourney',
  'OpenAI',
  'product recognition',
  ].join('|') + ')\\b'

const covidRegex = '\\b(?:' + [
  'covid', 'coronavirus', 'CoV2', 'CoV-2',
  'H1N1', 'N95', 'nCoV', 'pandemic',
  'Russian vaccine',
  'SARS',
  ].map(s => s.replace(/\./g, '\\.')).join('|') + '|face[- ]?mask|school.*reopen)\\b'

const environmentRegex = '\\b(?:' + [
  'agriculture', 'climate', 'carbon dioxide', 'CO2', 'coal',
  'EPA', 'electric vehicles?', 'electricity-storing',
  'methane',
  'pollution',
  'solar power', 'wind turbines?',
  ].join('|') + ')\\b'

const longReadsRegex = '\\b(?:' + [
  'aeon.co', 'longreads.com', 'nautil.us', 'newyorker.com', 'lrb.co.uk', 'theatlantic.com',
  ].map(s => s.replace(/\./g, '\\.')).join('|') + ')\\b'

const newsRegex = '\\b(?:' + [
  'abc.net.au', 'apnews.com',
  'bbc.com', 'bbc.co.uk', 'bloomberg.com', 'bnnbloomberg.ca', 'businessinsider.com',
  'cbc.ca', 'cnn.com',
  'euronews.com',
  'forbes.com',
  'guardian.co.uk', 'theguardian.com',
  'latimes.com',
  'nytimes.com',
  'reuters.com',
  'telegraph.co.uk',
  'washingtonpost.com',
  ].map(s => s.replace(/\./g, '\\.')).join('|') + ')\\b'

const faangRegex = '\\b(?:' + [
  'Amazon', 'AWS',
  'Apple', 'Tim Cook',
  'Facebook', 'Zuckerberg',
  'Google',
  'Microsoft',
  ].map(s => s.replace(/\./g, '\\.')).join('|') + ')\\b'

const uxWebRegex = '\\b(?:' + [
  'UX',
  'CSS',
  'Flutter', 'front-end',
  'React(?:\\.?js)?',
  '[Tt]ouch [Ii]nteraction',
  'Vue(?:\\.?js)?',
  ].join('|') + ')\\b'

const programmingRegex = '(?:^|[^a-z.])C[\' ]|\\b(?:' + [
  'algorithms?',
  'basic interpreter', 'bloom[- ]filter', 'BuckleScript',
  'C#', 'C[+][+][ ]*', 'code ?base', 'co-?routines?',
  'data structures?', 'Django',
  'Elixir',
  'GraphQL', 'Go [1-9].[0-9]+',
  'Haskell',
  'Java', 'javascript', 'json', 'Julia', 'JuliaDB',
  'golang',
  'Kotlin',
  'interlisp', 'lisp', 'LSM',
  'OOP',
  'programming', 'performant', 'Python',
  'Ruby', 'Rust',
  'Scala', 'SQL',
  'threads', 'typescript', 'type-checker',
  ].join('|') + ')\\b'

const devopsRegex = '\\b(?:' + [
  'AWS', 'Azure', 'GCP',
  'CI/CD', 'Datadog', 'DevOps', 'Docker', 'Grafana',
  'serverless',
  ].join('|') + ')\\b'

const linuxFreeSoftware = '\\b(?:' + [
  'EFF',
  'Firefox', 'freedom is not simple', 'FSF',
  'KDE',
  'Linux',
  'Mozilla',
  'Stallman', 'software\b.*\bfree',
  'Ubuntu',
  ].join('|') + ')\\b'

const privacyRegex = '\\b(?:' + [
  'device data',
  'end-to-end encryption',
  'https',
  'NSA',
  'personal data', 'privacy',
  'Sidewalk Labs', 'SSL', 'surveillance',
  'TLS',
].join('|') + ')\\b'

const blockchainRegex = '\\b(?:' + [
  'Bitcoin',
  'blockchain',
  'Coinbase',
  'crypto|cryptocurrency|cryptocurrencies',
  'Ethereum',
  'FTX',
  'NFT',
].join('|') + ')\\b'

const spaceTimeRegExp = '\\b(?:' + [
  'Arecibo',
  'extraterrestrial',
  'Hayabusa2',
  'periodic table', 'planet',
  'quantum',
  'space-?time', 'SpaceX', 'supernova',
  ].join('|') + ')\\b'

const lifeScienceRegex = '\\b(?:' + [
  'alzheimer', 'blood', 'cancer', 'dementia',
  'microbes?', 'mitochondria', 'phenotypes?',
  'vitamin',
  ].join('|') + ')\\b'

const societyRegex = '\\b(?:' + [
  'dark triad',
  'racist',
  'social', 'sociopaths?',
  'trying to do good',
  ].join('|') + ')\\b'

const businessRegex = '\\b(?:' + [
  '% stake',
  'acquired by',
  'for \\$',
  'in equity',
  ].join('|') + ')\\b'

const financeBankingRegex = '\\b(?:' + [
  'SVB',
  'Silicon Valley Bank',
  'USDC',
  ].join('|') + ')\\b'

new Vue({
  el: '#app',
  data: {
    store: {
      filterDate: 'today',
      tags: [
        { name: 'Show HN', type: 'show', regex: /^(?:Show HN|Launch HN)/i },
        { name: 'Ask HN', type: 'ask', regex: /^Ask HN/i },
        { name: 'AI, ML', type: 'title', regex: new RegExp(aiMlRegex, 'i') },
        { name: 'UX, mobile, web', type: 'title', regex: new RegExp(uxWebRegex) },
        { name: 'wiki', type: 'domain', regex: /\b(?:en\.wikipedia\.org|wiki\.c2\.com)$/i },
//      { name: 'programming', type: 'title', regex: new RegExp(programmingRegex, 'i') },
        { name: 'devops, systems', type: 'title', regex: new RegExp(devopsRegex, 'i') },
        { name: 'game', type: 'title', regex: /\b(?:chess|Fortnite|games?|gaming|sprites?)\b/i },
        { name: 'CPU, GPU, APU', type: 'title',
          regex: /\b(?:AMD|ARM|Intel|nVidia|Qualcomm|Snapdragon)\b/i },
        { name: 'Kubernetes', type: 'title', regex: /\b(?:kubernetes|k8s)\b/i },
        { name: 'llvm', type: 'title', regex: /\b(?:GraalVM|Graal|LLVM)\b/i },
//      { name: 'Linux, free software', type: 'title', regex: new RegExp(linuxFreeSoftware, 'i') },
        { name: 'Android, iPhone', type: 'title', regex: /\b(?:Android|iPhones?|Surface Duo)\b/i },
        { name: 'privacy', type: 'title', regex: new RegExp(privacyRegex, 'i') },
        { name: 'cryptocurrency', type: 'title', regex: new RegExp(blockchainRegex, 'i') },
        { name: 'hardware', type: 'title', regex: /\bFPGA|ISA|verilog\b/ },
//      { name: 'github.com', type: 'domain', regex: /\b(?:github\.com)$/i },
        { name: 'space-time', type: 'title', regex: new RegExp(spaceTimeRegExp, 'i') },
        { name: 'environment', type: 'title', regex: new RegExp(environmentRegex, 'i') },
        { name: 'life science', type: 'title', regex: new RegExp(lifeScienceRegex, 'i') },
        { name: 'Covid-19', type: 'title', regex: new RegExp(covidRegex, 'i') },
        { name: 'gig work', type: 'title', regex: /\b(?:Airbnb|Lyft|Uber|gig workers?)\b/i },
        { name: 'TikTok, WeChat', type: 'title', regex: /\b(?:ByteDance|TikTok|WeChat)\b/ },
        { name: 'Boeing, TSA', type: 'title', regex: /\b(?:Boeing|TSA)\b/i },
        { name: 'Huawei', type: 'title', regex: /\b(?:Huawei)\b/i },
        { name: '5G', type: 'title', regex: /\b(?:5G)\b/i },
        { name: 'Society', type: 'title', regex: new RegExp(societyRegex, 'i') },
        { name: 'Instagram', type: 'title', regex: /\b(?:Instagram)\b/i },
        { name: 'Twitter', type: 'title', regex: /\b(?:Twitter)\b/i },
        { name: 'Finance, Banking', type: 'title', regex: new RegExp(financeBankingRegex, 'i') },
        { name: 'Business', type: 'title', regex: new RegExp(businessRegex, 'i') },
        { name: 'FAAMG', type: 'title', regex: new RegExp(faangRegex, 'i') },
        { name: 'Trump', type: 'title', regex: /\b(?:Trump)\b/ },
        { name: 'India', type: 'title', regex: /\b(?:India|Bengaluru|Bangalore)\b/i },
        { name: 'Russia', type: 'title', regex: /\b(?:Russia)\b/i },
        { name: 'China, Hong Kong, Taiwan', type: 'title',
          regex: /\b(?:China|Chinese|Hong Kong|Taiwan)\b/i },
//      { name: 'archive.org', type: 'domain', regex: /\b(?:archive\.org)$/i },
//      { name: 'arstechnica.com', type: 'domain', regex: /\b(?:arstechnica\.com)$/i },
//      { name: 'medium.com', type: 'domain', regex: /\b(?:medium\.com)$/i },
        { name: 'long reads', type: 'domain', regex: new RegExp(longReadsRegex, 'i') },
        { name: 'general news', type: 'domain', regex: new RegExp(newsRegex, 'i') },
      ],
      tagStories: {},
      newStories: [],
      topStories: [],
      bestStories: [],
      showStories: [],
      askStories: [],
      jobStories: [],
      comments: {},
      items: {},
      users: {},
    },
  },
  router,
  render: h => h(App),
}).$mount('#app')
